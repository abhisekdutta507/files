1. What is Python?
Answer: It is a High-level cross-platform programming language.

2. Install PIP using Python3
Answer:
sudo apt update
sudo apt install python3-pip
pip3 --version

3. Start project using Code Runner
Answer: Ctrl + Alt + N

4. What are the Python Implementations?
Answer: CPython (Written in C), Jython (Written in Java), IronPython (Written in C#), PyPy (Subset of Python)

5. What is an Expression?
Answer: It is a piece of code that producces a value. For eg. 

print("*" * 3) the value becomes "***"

6. What is a Syntax Error?
Answer: A syntax error is a kind of error that is due to bad syntax or bad grammer in the code.

7. What dows a Linter do?
Answer: A linter is a tool that checks our code for potential errors. Mostyly in the category of syntactical errors. So if we have grammatical issues in our code, the linter will tell us before running our program.

8. What is PEP?
Answer: Python Enhancement Proposal. Code formatting guides.

9. What are the Primitive types available in Python?
Answer: There are 3 types of Primitives available in Python. This are number, boolean and string.

10. What are the 2 type of functions available in Python?
Answer: 1. functions that carried out a task
        2. functions that return some value (not None)

11. What is gurbage collection in Python?
Answer:

12. What is scoping in Python?
Answer: 1. local scope 2. global scope

13. How to setup breakpoints for debugging?
Answer: press F9 from keyboard to add breakpoints.
